import $ from 'jquery';

import "../css/main.scss";
import "../css/Pe-icon-7-stroke.css"
import "@fortawesome/fontawesome-free"

import "../js/tether.min.js";
import "../js/retina.min.js";
import "../js/owl.carousel.min.js";
import "../js/webfont.js";