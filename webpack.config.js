var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .addEntry('app', './assets/js/app.js')
    .copyFiles({
        from: 'assets/img',
        to: 'img/[path][name].[hash:8].[ext]',
        pattern: /\.(png|jpg|jpeg)$/
    })
    .copyFiles({
        from: 'assets/fonts',
        to: 'fonts/[path][name].[ext]',
    })
    .enableSassLoader()
    .autoProvidejQuery()
    .enableSourceMaps(!Encore.isProduction());


module.exports = Encore.getWebpackConfig();